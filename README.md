<img src="./834_2.png" />

# Garuda Linux
This is source code of Garuda Linux website found at [https://garudalinux.org/](https://garudalinux.org/).


## Table of Contents
- [How to contribute?](#how-to-contribute)
- [Report a bug](#report-a-bug)
- [Repo Maintainers](#maintainers)
- [Contact](#contact)
- [Frequently asked questions](#faq)

## How to Contribute?
You can browse through existing issues or open a new issue  and submit a PR to fix that issue.
For detailed info read our [contribution guidelines](./CONTRIBUTING.md)

## Report a bug
If you encountered some problem on main website, check if issue has already been reported, if not, check [Reporting Bugs](https://wiki.garudalinux.org/en/reporting-bugs)

## Maintainers
- @librewish
- @SGSm
- @dal.delmonico
- @dr460nf1r3
- @Yorper
- @ngargg
- @RohitSingh107
- @TotallyNotElite
- @jonathon
- @PedroHLC
- @petsam
- @sumantv26

## Contact
Contact us on [Telegram](https://telegram.me/garudalinux), [Twitter](https://twitter.com/garudalinux) and [Facebook](https://www.facebook.com/garudalinux.org).

## FAQ
For answers to frequently asked questions visit our [forum page](https://forum.garudalinux.org/).
