var overlay = document.querySelector(".overlay")
var popup = document.querySelector(".popup")
var overflow = false;
function showPopup() {
    overlay.classList.toggle("active");
    popup.classList.toggle("show-popup");
    overflow = overflow ? false : true;
    hideOverflowWhenOverlayIsActive()
}

overlay.addEventListener('click', () => {
    showPopup()
})

function hideOverflowWhenOverlayIsActive() {
    if (overflow) {
        document.querySelector("body").style.overflow = 'hidden';
    }
    else {
        document.querySelector("body").style.overflow = 'visible';
    }
}